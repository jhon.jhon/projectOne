package com.example.jhon.proyectouno;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextWatcher;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentCrearEstudiante.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentCrearEstudiante#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCrearEstudiante extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private EditText edtCod, edtNom, edtNot, edtLat, edtLong;
    Button btnGuardar, btnSesion;

    private OnFragmentInteractionListener mListener;

    public FragmentCrearEstudiante() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentCrearEstudiante.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCrearEstudiante newInstance(String param1, String param2) {
        FragmentCrearEstudiante fragment = new FragmentCrearEstudiante();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_fragment_crear_estudiante,
                container, false);

        edtCod = (EditText) view.findViewById(R.id.edtCodigo);
        edtNom = (EditText) view.findViewById(R.id.edtNombre);
        edtNot = (EditText) view.findViewById(R.id.edtNota);
        edtLat= (EditText) view.findViewById(R.id.edtLatitud);
        edtLong = (EditText) view.findViewById(R.id.edtLongitud);

        edtCod.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(edtCod);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        edtNom.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(edtNom);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        edtNot.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(edtNot);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        edtLat.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(edtNot);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        edtLong.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(edtNot);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        btnGuardar = (Button) view.findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);

        btnSesion = (Button) view.findViewById(R.id.btnSesion);

        btnSesion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            Intent intent = new Intent(getContext().getApplicationContext(), Login.class);
            getContext().startActivity(intent);
            Toast.makeText(getActivity(), "Login", Toast.LENGTH_LONG).show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onClick(View v) {

        if ( checkValidation () )
            submitForm();
        else
            Toast.makeText(getActivity(), "Completa los datos", Toast.LENGTH_LONG).show();

    }

    private void submitForm() {
        // Submit your form here. your form is valid
        DatabaseHandler db = new DatabaseHandler(this.getContext());
        //DatabaseHandler db = new DatabaseHandler();

        Estudiante estudiante= new Estudiante(Integer.parseInt(edtCod.getText().toString()),
                edtNom.getText().toString(), Float.parseFloat(edtNot.getText().toString()),
                Float.parseFloat(edtLat.getText().toString()),
                Float.parseFloat(edtLong.getText().toString()));

        db.insertEstudiante(estudiante);

        cargarListEstudiantes();

    }

    public void cargarListEstudiantes(){
        ListFragmentStudent list_estudiantes = new ListFragmentStudent();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.contenedorFragment, list_estudiantes);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.hasText(edtCod)) ret = false;
        if (!Validation.hasText(edtNom)) ret = false;
        if (!Validation.hasText(edtNot)) ret = false;
        if (!Validation.hasText(edtLat)) ret = false;
        if (!Validation.hasText(edtLong)) ret = false;
        return ret;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
