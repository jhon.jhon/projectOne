package com.example.jhon.proyectouno;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.FloatRange;
import android.util.Log;

/**
 * Created by jhon on 27/08/17.
 */

public class DatabaseHandler{

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "proyecto";
    private static final String TABLE_STUDENTS = "estudiantes";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_CODIGO = "codigo";
    private static final String COLUMN_NOMBRE = "nombre";
    private static final String COLUMN_NOTA = "nota";
    private static final String COLUMN_LATITUD = "latitud";
    private static final String COLUMN_LONGITUD= "longitud";

    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase database;


    public DatabaseHandler(Context aContext) {
        openHelper = new DatabaseOpenHelper(aContext);
        database = openHelper.getWritableDatabase();
    }


    public void insertEstudiante(Estudiante estudiante) {

        Log.d("El codigo: ", Integer.toString(estudiante.getCodigo()));
        Log.d("El nombre: ", estudiante.getNombre());
        Log.d("La nota: ", Float.toString(estudiante.getNota()));
        Log.d("La Longitud: ",  Float.toString(estudiante.getLongitud()));
        Log.d("La Latitud: ",  Float.toString(estudiante.getLatitud()));

        //abre la conexion, pone la bd en modo de edicion
        ContentValues values = new ContentValues();

        values.put(COLUMN_CODIGO, estudiante.getCodigo());
        values.put(COLUMN_NOMBRE,estudiante.getNombre());
        values.put(COLUMN_NOTA,estudiante.getNota());
        values.put(COLUMN_LATITUD,estudiante.getLatitud());
        values.put(COLUMN_LONGITUD,estudiante.getLongitud());

        //SQLiteDatabase db = this.getWritableDatabase();
        // Inserting Row
        database.insert(TABLE_STUDENTS, null, values);
        database.close(); // Closing database connection
    }

    public Cursor getData() {
        // TODO Auto-generated method stub
        //String columns[] = {COLUMN_CODIGO, COLUMN_NOMBRE, COLUMN_NOTA, COLUMN_LATITUD, COLUMN_LONGITUD };

        String query = "SELECT * FROM "+ TABLE_STUDENTS +  " ORDER BY " + COLUMN_ID + " ASC";

        //SQLiteDatabase db = this.getWritableDatabase();

        //Cursor c = db.rawQuery(query, null);
        //Cursor c = db.query(TABLE_STUDENTS, columns, null, null, null, null, null);

        return database.rawQuery(query, null);
    }

    public int deleteRow(String id) {
        return database.delete(TABLE_STUDENTS, COLUMN_ID + "=" + id, null);
    }

    public int updateRow(String id, int codigo, String nombre, float nota, float latitud, float longitud) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CODIGO, codigo);
        cv.put(COLUMN_NOMBRE, nombre);
        cv.put(COLUMN_NOTA, nota);
        cv.put(COLUMN_LATITUD, latitud);
        cv.put(COLUMN_LONGITUD, longitud);

        return database.update(TABLE_STUDENTS, cv, COLUMN_ID + "=" + id, null);
    }

    public Estudiante getEstudiante(String id) {
        System.out.println("id = " + id);


        Estudiante estudiante;
        String[] allColumns = {COLUMN_ID, COLUMN_CODIGO, COLUMN_NOMBRE,
                COLUMN_NOTA,COLUMN_LONGITUD, COLUMN_LATITUD};
        Cursor cursor = database.query(TABLE_STUDENTS, allColumns, "_id="+id ,null,null,null,null);

        cursor.moveToFirst();
        estudiante = cursorToStudent(cursor);
        cursor.close();
        return estudiante;

    }

    private Estudiante cursorToStudent(Cursor cursor) {
        //Se inicializa la clase
        Estudiante estudiante = new Estudiante();
        //Se asignan los valores
        estudiante.setId(Integer.parseInt(cursor.getString(0)));
        estudiante.setCodigo(Integer.parseInt(cursor.getString(1)));
        estudiante.setNombre(cursor.getString(2));
        estudiante.setNota(Float.parseFloat(cursor.getString(3)));
        estudiante.setLongitud(Float.parseFloat(cursor.getString(4)));
        estudiante.setLatitud(Float.parseFloat(cursor.getString(5)));
        return estudiante;
    }


    private class DatabaseOpenHelper extends SQLiteOpenHelper {

        public DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            String buildSQL = "CREATE TABLE " + TABLE_STUDENTS + "( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_CODIGO + " INTEGER, " +
                    COLUMN_NOMBRE + " TEXT NOT NULL, " + COLUMN_NOTA +  " REAL NOT NULL, " + COLUMN_LATITUD +
                    " REAL NOT NULL, " + COLUMN_LONGITUD +  " REAL NOT NULL );";
            //ejecuta la query
            sqLiteDatabase.execSQL(buildSQL);


        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENTS);
            // Create tables again
            onCreate(sqLiteDatabase);

        }

    }

}
