package com.example.jhon.proyectouno;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ShowEstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_est);
        Bundle theId = getIntent().getExtras();

        if (theId != null) {

            final TextView txtEditarNombre=(TextView)findViewById(R.id.txtNombre);
            txtEditarNombre.setText(theId.getString("nombre"));
            final TextView txtEditarCodigo=(TextView)findViewById(R.id.txtCodigo);
            txtEditarCodigo.setText(theId.getString("codigo"));
            final TextView txtEditarNota=(TextView)findViewById(R.id.txtNota);
            txtEditarNota.setText(theId.getString("nota"));
            final TextView txtEditLatitud=(TextView)findViewById(R.id.txtLatitud);
            txtEditLatitud.setText(theId.getString("latitud"));
            final TextView txtEditLongitud=(TextView)findViewById(R.id.txtLongitud);
            txtEditLongitud.setText(theId.getString("longitud"));

        }
    }

}
