package com.example.jhon.proyectouno;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateEstActivity extends AppCompatActivity {

    EditText edtCod, edtNom, edtNot, edtLon, edtLat;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Bundle theId = getIntent().getExtras();

        if (theId != null) {
            id = theId.getString("id");

            final EditText txtEditarNombre=(EditText)findViewById(R.id.edtSNombre);
            txtEditarNombre.setText(theId.getString("nombre"));
            final EditText txtEditarCodigo=(EditText)findViewById(R.id.edtSCodigo);
            txtEditarCodigo.setText(theId.getString("codigo"));
            final EditText txtEditarNota=(EditText)findViewById(R.id.edtSNota);
            txtEditarNota.setText(theId.getString("nota"));
            final EditText txtEditLatitud=(EditText)findViewById(R.id.edtSLatitud);
            txtEditLatitud.setText(theId.getString("latitud"));
            final EditText txtEditLongitud=(EditText)findViewById(R.id.edtSLongitud);
            txtEditLongitud.setText(theId.getString("longitud"));

        }

        edtCod = (EditText) findViewById(R.id.edtSCodigo);
        edtNom = (EditText) findViewById(R.id.edtSNombre);
        edtNot = (EditText) findViewById(R.id.edtSNota);
        edtLat = (EditText) findViewById(R.id.edtSLatitud);
        edtLon = (EditText) findViewById(R.id.edtSLongitud);

        String lawyerId = getIntent().getStringExtra(theId.getString("id"));

    }

    public void actualizarEstudiante(View view) {
        String codigo = edtCod.getText().toString();
        String nombre = edtNom.getText().toString();
        String nota = edtNot.getText().toString();
        String longitud = edtLon.getText().toString();
        String latitud = edtLat.getText().toString();

        System.out.print("Codigo:: " + codigo + " nombre:: " + nombre);


        if (codigo.length() != 0 && nombre.length() != 0 && nota.length() != 0 &&
                longitud.length() != 0 && latitud.length() != 0) {

            System.out.print("Codigo:: " + codigo + " nombre:: " + nombre);

            DatabaseHandler databaseHelper = new DatabaseHandler(this);
            databaseHelper.updateRow(id, Integer.parseInt(codigo), nombre, Float.parseFloat(nota),
                    Float.parseFloat(longitud), Float.parseFloat(latitud));

            Intent newIntent = new Intent(getBaseContext(), MainActivity.class);;
            startActivity(newIntent);
            Toast.makeText(this, "se ha actualizado con exito", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "debe ingresar datos", Toast.LENGTH_SHORT).show();
        }
    }

}
