package com.example.jhon.proyectouno;

/**
 * Created by jhon on 16/09/17.
 */

public class Usuario {

    private String nombreComplet;
    private String id;

    public String getNombreComplet() {
        return nombreComplet;
    }

    public void setNombreComplet(String nombreComplet) {
        this.nombreComplet = nombreComplet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Usuario() {

    }
}
