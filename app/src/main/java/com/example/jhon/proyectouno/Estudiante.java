package com.example.jhon.proyectouno;

/**
 * Created by jhon on 28/08/17.
 */

public class Estudiante {

    private int id;
    private int codigo;
    private String nombre;
    private float nota;
    private float latitud;
    private float longitud;


    public Estudiante(int codigo, String nombre, float nota, float latitud, float longitud) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.nota = nota;
        this.latitud = latitud;
        this.longitud = longitud;
    }
    public Estudiante(){

    }
    public int getId (){
        return id;
    }

    public void setId (int id){
        this.id = id;
    }

    public int getCodigo (){
        return codigo;
    }

    public void setCodigo (int codigo){
        this.codigo = codigo;
    }

    public String getNombre (){
        return nombre;
    }

    public void setNombre (String nombre){
        this.nombre = nombre;
    }

    public float getNota (){
        return nota;
    }

    public void setNota (float nota){
        this.nota = nota;
    }

    public float getLongitud (){
        return longitud;
    }

    public void setLongitud (float longitud){
        this.longitud = longitud;
    }

    public float getLatitud (){
        return latitud;
    }

    public void setLatitud (float latitud){
        this.latitud = latitud;
    }

    @Override
    public String toString() {
        return this.codigo + " " + this.nombre;
    }

}
