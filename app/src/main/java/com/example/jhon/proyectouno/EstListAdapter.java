package com.example.jhon.proyectouno;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by jhon on 10/09/17.
 */

public class EstListAdapter extends CursorAdapter {



    @SuppressWarnings("deprecation")
    public EstListAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        return inflater.inflate(R.layout.adapter_list_layout, viewGroup,false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        TextView id = (TextView) view.findViewById(R.id.textId);
        id.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));

        TextView codigo = (TextView) view.findViewById(R.id.textCod);
        codigo.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));

        TextView nombre = (TextView) view.findViewById(R.id.textNom);
        nombre.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));


        Button btnShow = (Button) view.findViewById(R.id.btnShow);
        Button btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        Button btnDelete = (Button) view.findViewById(R.id.btnDelete);

        btnUpdate.setTag(id.getText().toString());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, view.getTag().toString(), Toast.LENGTH_LONG).show();

                Estudiante objestudiante;
                DatabaseHandler databaseHelper = new DatabaseHandler(context);
                objestudiante = databaseHelper.getEstudiante(view.getTag().toString());
                Intent intent = new Intent( context.getApplicationContext() ,UpdateEstActivity.class);
                intent.putExtra("id", Integer.toString(objestudiante.getId()));
                intent.putExtra("codigo",Integer.toString(objestudiante.getCodigo()));
                intent.putExtra("nombre", objestudiante.getNombre());
                intent.putExtra("nota", Float.toString(objestudiante.getNota()));
                intent.putExtra("longitud", Float.toString(objestudiante.getLongitud()));
                intent.putExtra("latitud", Float.toString(objestudiante.getLatitud()));

                context.startActivity(intent);
            }
        });

        btnShow.setTag(id.getText().toString());
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, view.getTag().toString(), Toast.LENGTH_LONG).show();

                Estudiante objestudiante;
                DatabaseHandler databaseHelper = new DatabaseHandler(context);
                objestudiante = databaseHelper.getEstudiante(view.getTag().toString());

                Intent intent = new Intent( context.getApplicationContext() , ShowEstActivity.class);

                intent.putExtra("id", Integer.toString(objestudiante.getId()));
                intent.putExtra("codigo",Integer.toString(objestudiante.getCodigo()));
                intent.putExtra("nombre", objestudiante.getNombre());
                intent.putExtra("nota", Float.toString(objestudiante.getNota()));
                intent.putExtra("longitud", Float.toString(objestudiante.getLongitud()));
                intent.putExtra("latitud", Float.toString(objestudiante.getLatitud()));

                context.startActivity(intent);
            }
        });

        btnDelete.setTag(id.getText().toString());
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, view.getTag().toString(), Toast.LENGTH_LONG).show();

                DatabaseHandler databaseHelper = new DatabaseHandler(context);
                int result;
                result = databaseHelper.deleteRow(view.getTag().toString());
                context.startActivity(new Intent(context.getApplicationContext(), MainActivity.class));
                if (result == 1) {
                    Toast.makeText(context, "Se ha eliminado el estudiante", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(context, "No se ha eliminado", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}
