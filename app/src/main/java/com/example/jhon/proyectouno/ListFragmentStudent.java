package com.example.jhon.proyectouno;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListFragmentStudent.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListFragmentStudent#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragmentStudent extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EstListAdapter estListAdapter;
    DatabaseHandler databaseHelper;

    ListView listContent;
    TextView textCod;
    ArrayList<Estudiante> data = null;
    Button btnShow;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListFragmentStudent() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListFragmentStudent.
     */
    // TODO: Rename and change types and number of parameters
    public static ListFragmentStudent newInstance(String param1, String param2) {
        ListFragmentStudent fragment = new ListFragmentStudent();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list,
                container, false);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        databaseHelper = new DatabaseHandler(getActivity());

        estListAdapter = new EstListAdapter(getActivity(), databaseHelper.getData());
        setListAdapter(estListAdapter);
    }

}
